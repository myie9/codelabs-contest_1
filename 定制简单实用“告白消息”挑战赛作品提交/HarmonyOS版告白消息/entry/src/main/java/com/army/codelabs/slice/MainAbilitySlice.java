package com.army.codelabs.slice;

import com.army.codelabs.ResourceTable;
import com.huawei.agconnect.appmessaging.AGConnectAppMessaging;
import com.huawei.agconnect.appmessaging.AGConnectAppMessagingOnClickListener;
import com.huawei.agconnect.appmessaging.AGConnectAppMessagingOnDisplayListener;
import com.huawei.agconnect.appmessaging.model.Action;
import com.huawei.agconnect.appmessaging.model.AppMessage;
import com.huawei.agconnect.common.api.AGCInstanceID;
import com.huawei.agconnect.remoteconfig.AGConnectConfig;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.utils.net.Uri;

public class MainAbilitySlice extends AbilitySlice {
    // 单位为秒, 多少秒调用远程配置更新
    long fetchInterval = 0;
    private String flag = "purple";
    private AGConnectAppMessaging appMessaging;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        // 调用AGCInstanceID获取AAID
        getAAID();

        //配置应用内消息
        configMessage();

        // 读取远程配置
        remoteConfig();
    }

    /**
     * 调用AGCInstanceID获取AAID
     */
    private void getAAID() {
        // 手机调试, 获取到手机aaid, 然后在AGC上添加
        String aaid = AGCInstanceID.getInstance().getId();
        System.out.println("xx: " + aaid);
    }

    /**
     * 配置应用内消息
     */
    private void configMessage() {
        appMessaging = AGConnectAppMessaging.getInstance();
        // 设置是否允许同步AGC服务端数据
        appMessaging.setFetchMessageEnable(true);
        // 强制请求AGC服务端消息数据
        appMessaging.setForceFetch("AppOnForeground");
        // 设置是否允许展示消息
        appMessaging.setDisplayEnable(true);
        // 消息展示监听器
        appMessaging.addOnDisplayListener(new AGConnectAppMessagingOnDisplayListener() {
            public void onMessageDisplay(AppMessage param1AppMessage) {
                System.out.println("xx: Display Message Success!");
            }
        });
        // 消息点击监听器
        appMessaging.addOnClickListener(new AGConnectAppMessagingOnClickListener() {
            public void onMessageClick(AppMessage param1AppMessage, Action action) {
                String urlStr = action.getActionUrl();
                System.out.println("xx: getActionUrl: card url "+urlStr);

                Uri uri = Uri.parse(urlStr);
                System.out.println("xx: onMessageClick: message clicked");

            }
        });
    }

    /**
     * 读取远程配置
     */
    private void remoteConfig() {
        AGConnectConfig aGConnectConfig = AGConnectConfig.getInstance();
        aGConnectConfig.fetch(fetchInterval)
                .addOnSuccessListener(configValues -> {
                    System.out.println("xx: Fetch Success");
                    aGConnectConfig.apply(configValues);
                    // 获取远程配置条件管理
                    String value = aGConnectConfig.getValueAsString("theme_color");
                    System.out.println("xx: Theme Color " + value);

                    // 获取图片组件
                    Image img = findComponentById(ResourceTable.Id_imgHeader);
                    if (value.equals(flag)) {
                        img.setPixelMap(ResourceTable.Media_two);
                    }else {
                        img.setPixelMap(ResourceTable.Media_one);
                    }

                }).addOnFailureListener(e -> {
                    System.out.println("xx: Fetch Fail");
                });
    }
}
