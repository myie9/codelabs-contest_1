/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.haoc.lostandfound.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.haoc.lostandfound.R;
import com.haoc.lostandfound.clouddb.ThingEditFields;
import com.haoc.lostandfound.utils.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class EditActivity extends AppCompatActivity {
    public static final String ACTION_ADD = "com.haoc.lostandfound.clouddb.ADD";

    public static final String ACTION_SEARCH = "com.haoc.lostandfound.clouddb.SEARCH";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        initViews();
    }

    private void initViews() {
        Intent intent = getIntent();
        String action = intent.getAction();
        EditText thingNameEdit = findViewById(R.id.edit_thingname);

        Button addButton = findViewById(R.id.add);
        Button searchButton = findViewById(R.id.search);
        if (ACTION_ADD.equals(action)) {
            ThingEditFields.EditMode editMode = ThingEditFields.EditMode.valueOf(
                    intent.getStringExtra(ThingEditFields.EDIT_MODE));
            View fieldTime = findViewById(R.id.field_time);
            fieldTime.setVisibility(View.VISIBLE);
            EditText timeEdit = findViewById(R.id.edit_time);

            View fieldLocation = findViewById(R.id.field_location);
            fieldLocation.setVisibility(View.VISIBLE);
            EditText locationEdit = findViewById(R.id.edit_location);

            View fieldPhone = findViewById(R.id.field_phone);
            fieldPhone.setVisibility(View.VISIBLE);
            EditText phoneEdit = findViewById(R.id.edit_phone);
            Calendar calendar = Calendar.getInstance();
            //修改失物信息
            if (editMode == ThingEditFields.EditMode.MODIFY) {
                setTitle(R.string.edit_thing);
                thingNameEdit.setText(intent.getStringExtra(ThingEditFields.Thing_NAME));
                locationEdit.setText(intent.getStringExtra(ThingEditFields.LOCATION));
                phoneEdit.setText(intent.getStringExtra(ThingEditFields.PHONE));
                String Time = intent.getStringExtra(ThingEditFields.Time);
                Date date = DateUtils.parseDate(Time);
                calendar.setTime(date);
                timeEdit.setText(Time);
                addButton.setText(R.string.modify);
            }

            timeEdit.setOnClickListener(
                    v -> new DatePickerDialog(EditActivity.this, android.R.style.Theme_Material_Dialog_NoActionBar_MinWidth,
                            (view, year, month, dayOfMonth) -> {
                                // Month start from 0
                                String dateTime = year + "-" + (month + 1) + "-" + dayOfMonth;
                                calendar.set(year, month, dayOfMonth);
                                timeEdit.setText(dateTime);
                            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show());

            final int thingId = intent.getIntExtra(ThingEditFields.Thing_ID, -1);
            addButton.setOnClickListener(v -> {
                if ("".equals(thingNameEdit.getText().toString()) && "".equals(timeEdit.getText().toString())
                        && "".equals(locationEdit.getText().toString()) && "".equals(phoneEdit.getText().toString())) {
                    onBackPressed();
                    return;
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ThingEditFields.Thing_ID, thingId);
                resultIntent.putExtra(ThingEditFields.Thing_NAME, thingNameEdit.getText().toString());
                resultIntent.putExtra(ThingEditFields.Time, timeEdit.getText().toString());
                resultIntent.putExtra(ThingEditFields.LOCATION, locationEdit.getText().toString());
                resultIntent.putExtra(ThingEditFields.PHONE, phoneEdit.getText().toString());
                setResult(RESULT_OK, resultIntent);
                finish();
            });
            searchButton.setVisibility(View.GONE);
        } else if (ACTION_SEARCH.equals(action)) {
            setTitle(R.string.search_thing);
            View fieldShowCount = findViewById(R.id.field_show_count);
            fieldShowCount.setVisibility(View.VISIBLE);
            EditText showCountEdit = findViewById(R.id.edit_show_count);
            searchButton.setOnClickListener(v -> {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ThingEditFields.Thing_NAME, thingNameEdit.getText().toString());
                String showCount = showCountEdit.getText().toString();
                if (!showCount.isEmpty()) {
                    resultIntent.putExtra(ThingEditFields.SHOW_COUNT, Integer.parseInt(showCount));
                }
                setResult(RESULT_OK, resultIntent);
                finish();
            });
            addButton.setVisibility(View.GONE);
        } else {
            // Something wrong, just return
            finish();
            return;
        }

        Button cancelButton = findViewById(R.id.cancel);
        cancelButton.setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
