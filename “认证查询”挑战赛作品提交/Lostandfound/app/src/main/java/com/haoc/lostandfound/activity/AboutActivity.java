package com.haoc.lostandfound.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.haoc.lostandfound.R;

public class AboutActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        findViewById(R.id.imgView_back).setOnClickListener(this);
        findViewById(R.id.features).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgView_back:
                finish();
                break;
            case R.id.features:
                startActivity(new Intent(AboutActivity.this, FeaturesActivity.class));
                break;
        }
    }
}