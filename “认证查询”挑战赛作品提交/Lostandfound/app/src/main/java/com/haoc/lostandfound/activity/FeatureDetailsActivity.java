package com.haoc.lostandfound.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.haoc.lostandfound.R;

public class FeatureDetailsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature_details);
        TextView txtViewTitle = findViewById(R.id.title_feature);
        TextView txtViewDetails = findViewById(R.id.details_feature);
        txtViewTitle.setText(getIntent().getStringExtra("title_feature"));
        txtViewDetails.setText(getIntent().getStringExtra("details"));
        findViewById(R.id.imgView_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}