package com.haoc.lostandfound.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.haoc.lostandfound.R;
import com.haoc.lostandfound.adapter.FeatureAdapter;
import com.haoc.lostandfound.entity.FeatureEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

;

public class FeaturesActivity extends Activity {
    private List<FeatureEntity> mFeatureList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_features);
        setmFeatureList();
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        FeatureAdapter featureAdapter = new FeatureAdapter(mFeatureList);
        recyclerView.setAdapter(featureAdapter);
        featureAdapter.setOnItemClickListener(new FeatureAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Serializable obj) {
                FeatureEntity feature = (FeatureEntity) obj;
                String title_feature = feature.getTitleFeature();
                String details = feature.getDetails();
                Intent intent = new Intent(FeaturesActivity.this, FeatureDetailsActivity.class);
                intent.putExtra("title_feature", title_feature);
                intent.putExtra("details", details);
                startActivity(intent);
            }
        });

        findViewById(R.id.imgView_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setmFeatureList() {
        FeatureEntity v107 = new FeatureEntity("失物招领1.0.7主要更新", "04月30日", "1.新增功能介绍，可查看更新日志\n2.封装代码，减少代码冗余");
        mFeatureList.add(v107);
        FeatureEntity v106 = new FeatureEntity("失物招领1.0.6主要更新", "04月28日", "1.修复更新头像bug\n2.解决更新头像后不能即时刷新头像的问题\n3.解决一个用户更新头像导致所有用户更新头像的问题");
        mFeatureList.add(v106);
        FeatureEntity v105 = new FeatureEntity("失物招领1.0.5主要更新", "04月26日", "1.新增更新密码、重置密码功能\n2.优化显示效果，删除手机号前面的+86-\n3.优化操作失败时吐司的显示时长\n4.优化UI界面，新增关于页面并实现图标圆角");
        mFeatureList.add(v105);
        FeatureEntity v104 = new FeatureEntity("失物招领1.0.4主要更新", "04月25日", "1.接入云存储服务\n2.新增更新头像功能，通过选择本地照片，裁剪后上传至云存储并更新头像");
        mFeatureList.add(v104);
        FeatureEntity v103 = new FeatureEntity("失物招领1.0.3主要更新", "04月24日", "1.优化UI界面\n2.新增验证码倒计时，优化代码逻辑，修改最短发送时间为60s");
        mFeatureList.add(v103);
        FeatureEntity v102 = new FeatureEntity("失物招领1.0.2主要更新", "04月21日", "1.优化UI界面，新增一系列图标");
        mFeatureList.add(v102);
        FeatureEntity v101 = new FeatureEntity("失物招领1.0.1主要更新", "04月22日", "1.优化UI界面，新增沉浸式状态栏以及圆形头像");
        mFeatureList.add(v101);
        FeatureEntity v100 = new FeatureEntity("失物招领1.0.0主要更新", "04月20日", "1.接入认证服务，可通过手机号、邮箱、华为帐号、匿名四种方式登录\n2.移除需要手动输入Countrycode，自动默认为中国+86\n3.优化界面UI\n4.优化增删查改等代码逻辑");
        mFeatureList.add(v100);
    }
}