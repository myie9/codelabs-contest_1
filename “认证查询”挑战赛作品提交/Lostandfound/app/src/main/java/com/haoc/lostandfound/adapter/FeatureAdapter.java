package com.haoc.lostandfound.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.haoc.lostandfound.entity.FeatureEntity;
import com.haoc.lostandfound.R;

import java.io.Serializable;
import java.util.List;

/**
 * @description: 功能介绍适配器
 * @author: Haoc
 * @Time: 2022/4/29  11:26
 */

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.ViewHolder> {

    private List<FeatureEntity> mFeatureList;
    private static OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtViewTitle;
        private TextView txtViewDate;
        private FeatureEntity feature;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtViewTitle = view.findViewById(R.id.title_feature);
            txtViewDate = view.findViewById(R.id.date);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(feature);
                }
            });
        }
    }

    public FeatureAdapter(List<FeatureEntity> mFeatureList) {
        this.mFeatureList = mFeatureList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_feature_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FeatureEntity feature = mFeatureList.get(position);
        holder.txtViewTitle.setText(feature.getTitleFeature());
        holder.txtViewDate.setText(feature.getDate());
        holder.feature = feature;
    }

    @Override
    public int getItemCount() {
        if (mFeatureList != null && mFeatureList.size() > 0) {
            return mFeatureList.size();
        } else {
            return 0;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Serializable obj);
    }
}
