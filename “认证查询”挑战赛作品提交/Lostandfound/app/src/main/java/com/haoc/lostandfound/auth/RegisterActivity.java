package com.haoc.lostandfound.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.haoc.lostandfound.R;
import com.haoc.lostandfound.activity.MainActivity;
import com.haoc.lostandfound.utils.Myutils;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.EmailUser;
import com.huawei.agconnect.auth.PhoneUser;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.agconnect.auth.VerifyCodeSettings;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;

public class RegisterActivity extends Activity implements View.OnClickListener {
    private EditText accountEdit;
    private EditText passwordEdit;
    private EditText verifyCodeEdit;
    Button send;

    private LoginActivity.Type type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        type = (LoginActivity.Type) getIntent().getSerializableExtra("registerType");
        initView();
    }

    private void initView() {
        TextView accountTv = findViewById(R.id.tv_account);
        accountEdit = findViewById(R.id.et_account);
        passwordEdit = findViewById(R.id.et_password);
        verifyCodeEdit = findViewById(R.id.et_verify_code);
        if (type == LoginActivity.Type.EMAIL) {
            accountTv.setText(R.string.login_email);
        } else {
            accountTv.setText(R.string.phone_number);
        }

        Button registerBtn = findViewById(R.id.btn_register);
        registerBtn.setOnClickListener(this);

        send = findViewById(R.id.btn_send);
        send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                login();
                break;
            case R.id.btn_send:
                sendVerificationCode();
                break;
        }
    }

    private void login() {
        // 邮箱注册
        if (type == LoginActivity.Type.EMAIL) {
            String email = accountEdit.getText().toString().trim();
            String password = passwordEdit.getText().toString().trim();
            String verifyCode = verifyCodeEdit.getText().toString().trim();
            // build email user
            EmailUser emailUser = new EmailUser.Builder()
                    .setEmail(email)
                    .setPassword(password)//optional,if you set a password, you can log in directly using the password next time.
                    .setVerifyCode(verifyCode)
                    .build();
            // create email user
            AGConnectAuth.getInstance().createUser(emailUser)
                    .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                        @Override
                        public void onSuccess(SignInResult signInResult) {
                            // After a user is created, the user has logged in by default.
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            LoginActivity.loginActivity.finish();
                            finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(RegisterActivity.this, "createUser fail:" + e, Toast.LENGTH_LONG).show();
                        }
                    });
            // 手机号注册
        } else {
            String phoneNumber = accountEdit.getText().toString().trim();
            String password = passwordEdit.getText().toString().trim();
            String verifyCode = verifyCodeEdit.getText().toString().trim();
            // build phone user
            PhoneUser phoneUser = new PhoneUser.Builder()
                    .setCountryCode("86")
                    .setPhoneNumber(phoneNumber)
                    .setPassword(password)//可选
                    .setVerifyCode(verifyCode)
                    .build();
            // create phone user
            AGConnectAuth.getInstance().createUser(phoneUser)
                    .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                        @Override
                        public void onSuccess(SignInResult signInResult) {
                            // 创建帐号成功后，默认已登录
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            LoginActivity.loginActivity.finish();
                            finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(RegisterActivity.this, "createUser fail:" + e, Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void sendVerificationCode() {
        if (type == LoginActivity.Type.EMAIL) {
            String email = accountEdit.getText().toString().trim();
            Myutils.sendVerificationCodeByEmail(RegisterActivity.this, send, email, VerifyCodeSettings.ACTION_REGISTER_LOGIN);
        } else {
            String phoneNumber = accountEdit.getText().toString().trim();
            Myutils.sendVerificationCodeByPhoneNumber(RegisterActivity.this, send, phoneNumber, VerifyCodeSettings.ACTION_REGISTER_LOGIN);
        }
    }
}