package com.lzf.trace.publics.common;

import android.content.Context;

import com.lzf.trace.publics.utils.SharePreferenceUtils;



public class ServiceConfig {

    private static SharePreferenceUtils mSharePreferenceUtils;

    private static void initSharePerference(Context context) {
        if (mSharePreferenceUtils == null) {
            mSharePreferenceUtils = new SharePreferenceUtils(context,
                    Constants.SERVICE_CONFIG);
        }
    }

    /**
     * 服务器ip
     *
     * @param context
     * @return
     */
    public static String setServicrIp(Context context, String shareurl) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.SERVICE_IP, shareurl);
        return shareurl;
    }

    /**
     * 服务器ip
     *
     * @param context
     * @return
     */
    public static String getServicrIp(Context context) {
        initSharePerference(context);
        String servicrIp = (String) mSharePreferenceUtils.get(Constants.SERVICE_IP,
                Constants.SERVICE_IP_DEFAULT);
        return servicrIp;
    }

    /**
     * 服务器端口
     *
     * @param context
     * @return
     */
    public static String setServicrPort(Context context, String servicrPort) {
        initSharePerference(context);
        mSharePreferenceUtils.put(Constants.SERVICE_PORT, servicrPort);
        return servicrPort;
    }

    /**
     * 服务器端口
     *
     * @param context
     * @return
     */
    public static String getServicrPort(Context context) {
        initSharePerference(context);
        String servicrPort = (String) mSharePreferenceUtils.get(Constants.SERVICE_PORT,
                Constants.SERVICE_PORT_DEFAULT);
        return servicrPort;
    }


    /**
     * 退出程序
     *
     * @param context
     */
    public static void logOut(Context context) {
        initSharePerference(context);
        mSharePreferenceUtils.clear();
    }

}