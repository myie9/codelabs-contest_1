package com.lzf.trace.publics.utils;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class MarginDecoration extends RecyclerView.ItemDecoration {
    private int margin;
    private int style;

    public MarginDecoration(Context context, int style, int margin) {
        this.margin = margin;
        this.style = style;
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //1：条形（上下有距离）；2：方形（四周有距离）；3：主界面服务列表（上下左右有距离的条形）
        if (style == 1) {
            outRect.set(0, margin, 0, margin);
        }
        if (style == 2) {
            outRect.set(margin, margin, margin, margin);
        }
        if (style == 3) {
            outRect.set(margin, margin, margin, margin);
        }

    }
}