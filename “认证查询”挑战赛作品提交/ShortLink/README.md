# ShortLink
> ShortLink 无服务短链接活码管理平台,通过集成Serverless认证服务和云数据库服务,为用户提供安全认证和活码管理,用户可以管理和更新链接,保证链接及时有效,提供营销转换效率

本系统界面清晰,功能完整,操作简单，具有较强商业转化价值更佳

## 应用技术

+ AppGallery Connect 短信认证进程登录

+ 华为云数据库存储短链接地址

+ Ant Design Pro 前台管理页面

## 实现要点

+ 用户通过手机号进行认证登录,查询管理自己创建的链接

+ 数据库权限设置为创建者可修改,保证链接不会被更改

+ 通过设置主键使短链接不会重复

+ 查询匹配链接进行跳转

## 实现功能


+ 发送验证码  
+ 验证码登录  
+ 退出  

+ 创建短链接  
+ 修改短链接  
+ 删除短链接  

+ 短链接跳转


## 使用说明

下载源代码后:

```bash
npm install
```

或

```bash
yarn
```

进行依赖安装

### 调试项目

```bash
npm start
```

### 构建项目

```bash
npm run build
```

### 部署项目

构建后将dist目录下的静态文件托管至网页服务器即可

## 关于

[gitee开源代码](https://gitee.com/yance/codelabs-contest/tree/master/%E2%80%9C%E8%AE%A4%E8%AF%81%E6%9F%A5%E8%AF%A2%E2%80%9D%E6%8C%91%E6%88%98%E8%B5%9B%E4%BD%9C%E5%93%81%E6%8F%90%E4%BA%A4/ShortLink)

[效果演示](https://suo.pw)
