import { useIntl } from 'umi';
import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';

const Footer = () => {
  const intl = useIntl();
  const defaultMessage = '为您提供链接缩短服务';
  const currentYear = new Date().getFullYear();
  return (
    <DefaultFooter
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'suo.pw',
          title: '链接缩短',
          href: 'https://suo.pw',
          blankTarget: true,
        },
        {
          key: 'github',
          title: <GithubOutlined />,
          href: 'https://gitee.com/yance/codelabs-contest/tree/master/%E2%80%9C%E8%AE%A4%E8%AF%81%E6%9F%A5%E8%AF%A2%E2%80%9D%E6%8C%91%E6%88%98%E8%B5%9B%E4%BD%9C%E5%93%81%E6%8F%90%E4%BA%A4/ShortLink',
          blankTarget: true,
        },
        {
          key: '6op.cn',
          title: '速链',
          href: 'https://6op.cn',
          blankTarget: true,
        },
      ]}
    />
  );
};

export default Footer;
