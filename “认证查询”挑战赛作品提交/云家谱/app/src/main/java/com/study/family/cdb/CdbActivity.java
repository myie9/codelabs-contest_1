package com.study.family.cdb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.huawei.agconnect.AGCRoutePolicy;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptions;
import com.huawei.agconnect.AGConnectOptionsBuilder;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.study.family.R;
import com.study.family.model.CloudDBZoneWrapper;

public class CdbActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private BottomNavigationView mNavigationBar;
    public AGConnectCloudDB mCloudDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);
        mViewPager = findViewById(R.id.container);
        CdbActivity.SectionsPagerAdapter sectionsPagerAdapter = new CdbActivity.SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(sectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(mPageChangeListener);

        mNavigationBar = findViewById(R.id.nav_view);
        mNavigationBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        cloudDBstart();

    }
    //初始化数据库
    private void cloudDBstart() {
        CloudDBZoneWrapper.initAGConnectCloudDB(this);

        AGConnectAuth auth = AGConnectAuth.getInstance(AGConnectInstance.getInstance());
        mCloudDB = AGConnectCloudDB.getInstance(AGConnectInstance.getInstance(), auth);
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == CdbActivity.Page.HOME.pos) {
                 return HomePageFragment.newInstance();
            } else {
                 return AboutMeFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return CdbActivity.Page.values().length;
        }
    }
    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int i) {
            updateTitle();
            if (i == CdbActivity.Page.HOME.getPos()) {
                if (mNavigationBar.getSelectedItemId() == R.id.navigation_home) {
                    return;
                }
                mNavigationBar.setSelectedItemId(R.id.navigation_home);
            } else {
                if (mNavigationBar.getSelectedItemId() == R.id.navigation_about_me) {
                    return;
                }
                mNavigationBar.setSelectedItemId(R.id.navigation_about_me);
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }
    };
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            updateTitle();
            if (item.getItemId() == R.id.navigation_home) {
                if (mViewPager.getCurrentItem() == CdbActivity.Page.HOME.getPos()) {
                    return true;
                }
                mViewPager.setCurrentItem(CdbActivity.Page.HOME.getPos(), true);
            } else if (item.getItemId() == R.id.navigation_about_me) {
                if (mViewPager.getCurrentItem() == CdbActivity.Page.ABOUT_ME.getPos()) {
                    return true;
                }
                mViewPager.setCurrentItem(CdbActivity.Page.ABOUT_ME.getPos(), true);
            }
            return true;
        }
    };
    private void updateTitle() {
        if (mViewPager.getCurrentItem() == CdbActivity.Page.HOME.getPos()) {
            setTitle(getString(R.string.book_manager_title));
        } else {
            setTitle(getString(R.string.user_info_str));
        }
    }
    private enum Page {
        HOME(0),
        ABOUT_ME(1);

        private final int pos;

        Page(int pos) {
            this.pos = pos;
        }

        int getPos() {
            return pos;
        }
    }
    @Override
    protected void onResume() {
        updateTitle();
        super.onResume();
    }
    protected void hideNavigationBar() {
        mNavigationBar.setVisibility(View.GONE);
    }

    protected void showNavigationBar() {
        mNavigationBar.setVisibility(View.VISIBLE);
    }

    interface LinkClickCallback {
        void click();
    }
}