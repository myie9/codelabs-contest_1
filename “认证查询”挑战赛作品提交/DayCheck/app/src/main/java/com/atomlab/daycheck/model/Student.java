package com.atomlab.daycheck.model;

import com.bin.david.form.annotation.SmartColumn;
import com.bin.david.form.annotation.SmartTable;

import java.io.Serializable;

/**
 *
 *
 */
@SmartTable(name="健康信息列表")
public class Student implements Serializable {

    @SmartColumn(id =1,name = "id")
    private Integer id;

    @SmartColumn(id=2,name="姓名")
    private String name;

    @SmartColumn(id=3,name="体温")
    private Float temperature;

    @SmartColumn(id=4,name="是否绿码")
    private String green;

    @SmartColumn(id=5,name="疫苗次数")
    private Integer inoculation;


    @SmartColumn(id=6,name="电话")
    private String phone;

    @SmartColumn(id=7,name="地址")
    private String address;

    public Student(Integer id, String name, Float temperature, String green, Integer inoculation, String phone, String address) {
        this.id = id;
        this.name = name;
        this.temperature = temperature;
        this.green = green;
        this.inoculation = inoculation;
        this.phone = phone;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public String getGreen() {
        return green;
    }

    public void setGreen(String green) {
        this.green = green;
    }

    public Integer getInoculation() {
        return inoculation;
    }

    public void setInoculation(Integer inoculation) {
        this.inoculation = inoculation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
