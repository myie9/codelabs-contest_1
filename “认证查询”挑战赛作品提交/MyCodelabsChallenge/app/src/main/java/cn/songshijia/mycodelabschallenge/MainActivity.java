package cn.songshijia.mycodelabschallenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.auth.AGConnectAuthCredential;
import com.huawei.agconnect.auth.AGConnectUser;
import com.huawei.agconnect.auth.PhoneAuthProvider;
import com.huawei.agconnect.auth.SignInResult;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText phoneNumber = findViewById(R.id.phone_number);
        EditText password = findViewById(R.id.password);
        Context context = this;
        findViewById(R.id.login).setOnClickListener(v -> {
            String phone = phoneNumber.getText().toString();
            String passwd = password.getText().toString();
            if (phone.length() > 0 && passwd.length() > 0) {
                AGConnectAuthCredential credential = PhoneAuthProvider.credentialWithPassword("+86",
                        phone, passwd);
                AGConnectAuth.getInstance().signIn(credential)
                        .addOnSuccessListener(new OnSuccessListener<SignInResult>() {
                            @Override
                            public void onSuccess(SignInResult signInResult) {
                                //获取登录信息
                                System.out.println("登录成功:" + signInResult.getUser().getUid());
                                Toast.makeText(context, "登录成功", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setClass(context, DataActivity.class);
                                startActivity(intent);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, e.getMessage() + "：登录失败",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
            } else {
                Toast.makeText(this, "手机号码或密码为空，登陆失败", Toast.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.register).setOnClickListener(v -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        });

        AGConnectUser user = AGConnectAuth.getInstance().getCurrentUser();
        if (user != null) {
            System.out.println(user.getPhone() + "已登录");
            Toast.makeText(context, user.getPhone() + "已登录", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClass(this, DataActivity.class);
            startActivity(intent);
        }
    }
}